#include <iostream>
#include <string>
#include <vector>
#include <ctime>

class Animal
{
public:
    explicit Animal(std::string val) : voice_str(val) {};

    void voice()
    {
        std::cout << voice_str << std::endl;
    }

private:
    std::string voice_str = "not set";
};

class Cat : public Animal
{
public:
    Cat() : Animal("Meow") {};
};

class Dog : public Animal
{
public:
    Dog() : Animal("Woof") {};
};

class Cthulhu : public Animal
{
public:
    Cthulhu() : Animal("... (it sleeps)") {};
};

int main()
{
    std::vector<Animal> animals;

    std::srand(std::time(nullptr));
    for (size_t i = 0; i < 10; ++i)
    {
        size_t choice = std::rand() % 3 + 1;
        switch (choice)
        {
        case 1:
        {
            animals.emplace_back(Cat());
            break;
        }
        case 2:
        {
            animals.emplace_back(Dog());
            break;
        }
        case 3:
        {
            animals.emplace_back(Cthulhu());
            break;
        }
        default:
            break;
        }
    }
    for (Animal &a : animals)
    {
        a.voice();
    }
}
